;; This is a basic Falcon mode support
;; Thanks to:
;;
;; http://www.emacswiki.org/emacs/ModeTutorial

(require 'cc-mode)

(defvar falcon-mode-hook nil)

(defvar falcon-mode-map
  (let ((map (make-keymap)))
    (define-key map "\C-j" 'newline-and-indent)
    map)
  "Keymap for Falcon major mode")

(add-to-list 'auto-mode-alist '("\\.fal\\'" . falcon-mode))

(defconst falcon-font-lock-keywords
  (list '("\\<\\(?:a\\(?:nd\\|s\\)\\|break\\|c\\(?:a\\(?:se\\|tch\\)\\|lass\\|on\\(?:st\\|tinue\\)\\)\\|directive\\|e\\(?:l\\(?:if\\|se\\)\\|n\\(?:d\\|um\\)\\|xport\\)\\|f\\(?:or\\(?:first\\|last\\|middle\\)?\\|rom\\|unction\\)\\|i\\(?:\\(?:mpor\\|ni\\)t\\|[fn]\\)\\|l\\(?:ang\\|o\\(?:ad\\|op\\)\\)\\|not\\(?:in\\)?\\|o\\(?:bject\\|ff\\|[nr]\\)\\|provides\\|s\\(?:el\\(?:ect\\|f\\)\\|t\\(?:ep\\|rict\\)\\|witch\\)\\|t\\(?:o\\|ry\\)\\|version\\|while\\)?\\>" . font-lock-builtin-face)
	'("\\<\\(?:f\\(?:alse\\|self\\)\\|nil\\|self\\|true\\)\\>" . font-lock-constant-face))
  "Falcon keywords")


(defun falcon-indent-line ()
  "Indent current line as FALCON code."
  (interactive)
  (beginning-of-line)
  (if (bobp)
      (indent-line-to 0)		   ; First line is always non-indented
    (let ((not-indented t) cur-indent)
      (if (looking-at "^[ \t]*\\(end\\|elif\\|else\\)") ; If the line we are looking at is the end of a block, then decrease the indentation
	  (progn
	    (save-excursion
	      (forward-line -1)
	      (setq cur-indent (- (current-indentation) default-tab-width)))
	    (if (< cur-indent 0) ; We can't indent past the left margin
		(setq cur-indent 0)))
	(save-excursion
	  (while not-indented ; Iterate backwards until we find an indentation hint
	    (forward-line -1)
	    (if (looking-at "^[ \t]*end") ; This hint indicates that we need to indent at the level of the END_ token
		(progn
		  (setq cur-indent (current-indentation))
		  (setq not-indented nil))
	      (if (looking-at "^[ \t]*\\(class\\|function\\|object\\|for\\|enum\\|loop\\|switch\\|try\\|\\if\\)") ; This hint indicates that we need to indent an extra level
		  (progn
		    (setq cur-indent (+ (current-indentation) default-tab-width)) ; Do the actual indenting
		    (setq not-indented nil))
		(if (bobp)
		    (setq not-indented nil)))))))
      (if cur-indent
	  (indent-line-to cur-indent)
	(indent-line-to 0))))) 

; If we didn't see an indentation hint, then allow no indentation


(defvar falcon-mode-syntax-table
  (let ((table (make-syntax-table)))
    (c-populate-syntax-table table)
    (modify-syntax-entry ?$ "_" table)
    table)
  "Syntax table for `falcon-mode'.")


(defun falcon-mode ()
  (interactive)
  (kill-all-local-variables)
  (use-local-map falcon-mode-map)
  (set-syntax-table falcon-mode-syntax-table)
  ;; Set up font-lock
  (set (make-local-variable 'font-lock-defaults) '(falcon-font-lock-keywords))
  ;; Register our indentation function
  (set (make-local-variable 'indent-line-function) 'falcon-indent-line)  
  (setq major-mode 'falcon-mode)
  (setq mode-name "falcon")
  (run-hooks 'falcon-mode-hook))

(provide 'falcon-mode)

;; First level keywords
;; (regexp-opt '("load" "import" "from" "self" "as" "export" "directive" "lang" "strict" "version" "on" "off" "and" "or" "in" "notin" "provides" "not" "function" "end" "enum" "class" "init" "" "if" "elif" "else" "while" "for" "to" "step" "loop" "continue" "break" "forfirst" "formiddle" "forlast" "switch" "select" "case" "try" "catch" "end" "const" "object"))

;; Second level keywords
;; (regexp-opt '("nil" "true" "false" "self" "fself"))
